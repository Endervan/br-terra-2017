

<?php if (Url::getURL( 0 ) == "" or Url::getURL( 0 ) == "produtos") :?>
  <div class="container">
    <div class="row justify-content-md-center titulo_equipamento">
      <div class="col-8 top20">
        <h6><span>CONFIRA NOSSA LINHA DE </span></h6>
        <h3>EQUIPAMENTOS</h3>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo.png" alt="">
      </div>
      <div class="col-2 p-0 top15">
        <a class="btn btn-default btn_cinza" href="<?php echo Util::caminho_projeto() ?>/orcamento" title="" >
          SOLICITE ORÇAMENTO
        </a>
      </div>
    </div>
  </div>


<?php else: ?>

  <?php if (Url::getURL( 0 ) == "empresa") :
    $topNegativo = "mt-90";
  endif; ?>


  <div class="container <?php echo $topNegativo; ?>">
    <div class="row justify-content-md-center titulo_equipamento">

      <?php if (Url::getURL( 0 ) == "servico" or Url::getURL( 0 ) == "produto") :?>

        <div class="col-10 top20">
          <h6><span>CONFIRA NOSSA LINHA DE </span></h6>
          <h3>EQUIPAMENTOS</h3>
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo.png" alt="">
        </div>

      <?php else: ?>


        <div class="col-10 text-right top20">
          <div class="col-6 text-left ml-auto">
            <h6 class="ml-5"><span>CONFIRA NOSSA LINHA DE</span></h6>
            <h3 class="ml-5">EQUIPAMENTOS</h3>
          </div>
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo.png" alt="">
        </div>


      <?php endif; ?>

    </div>
  </div>

<?php endif; ?>


<?php if (Url::getURL( 0 ) == "" or Url::getURL( 0 ) == "servico"  or Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "produtos" or  Url::getURL( 0 ) == "empresa") :?>
  <!-- ======================================================================= -->
  <!--CATEGORIAS-->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_categorias">
    <div class="row ">

      <div class="container ">
        <div class="row justify-content-md-center empresa_home">



          <div class="col-10">
            <div class=" lista_categorias">
              <div class="col-12 padding0 text-center">
                <div id="slider1" class="flexslider">

                  <ul class="slides">
                    <?php
                    $i=0;
                    if (Url::getURL( 0 ) == "") :
                      $result1 = $obj_site->select("tb_categorias_produtos","and exibir_home = 'SIM'");
                      else :
                        $result1 = $obj_site->select("tb_categorias_produtos");
                      endif;
                      if (mysql_num_rows($result1) > 0) {
                        while ($row1 = mysql_fetch_array($result1)) {

                          ?>
                          <li class="text-center top20">
                            <a  href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>" >
                              <div class=""><span><?php Util::imprime($row1[titulo]); ?></span></div>
                            </a>
                          </li>
                          <?php
                        }

                      }


                      ?>

                      <li class="active top20">
                        <a class="todas_cat" href="<?php echo Util::caminho_projeto() ?>/produtos" title="" >
                          TODAS
                        </a>
                      </li>

                    </ul>
                  </div>
                </div>



              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!--CATEGORIAS-->
    <!-- ======================================================================= -->
  <?php endif; ?>


  <?php if (Url::getURL( 0 ) == "" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico" or  Url::getURL( 0 ) == "empresa") :?>
    <div class="container">
      <div class="row justify-content-md-center relativo">

        <div class="col-11 lista_produtos_home bottom20">
          <div id="slider_carousel" class="col-12 p-0 flexslider" >
            <ul class="slides">

              <?php
              $i=0;

              if (Url::getURL( 0 ) == "") :
                $result1 = $obj_site->select("tb_produtos","and exibir_home = 'SIM'");
                else :
                  $result1 = $obj_site->select("tb_produtos");
                endif;

                if (mysql_num_rows($result1) > 0) {

                  while ($row1 = mysql_fetch_array($result1)) {

                    ?>

                    <li class="col-4">
                      <div class="carroucel_produtos top25">
                        <div class="card relativo">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                            <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 293, 222, array("class"=>"w-100", "alt"=>"$row1[titulo]")) ?>
                          </a>
                          <div class="card-body bg_produtos_home">
                            <div class="desc_titulo_home"><h5 ><span><?php Util::imprime($row1[titulo],52); ?></span></h5></div>
                          </div>

                          <div class="produto-hover">
                            <div class="row row_alt align-items-center">
                              <a class="col-6  text-right " href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="Saiba Mais">
                                <i class="fas fa-plus-circle fa-4x" style=""></i>
                              </a>

                              <a class="col-6" href="javascript:void(0);"  title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row1[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                <i class="fas fa-shopping-cart fa-4x" style=""></i>
                              </a>
                            </div>
                          </div>

                        </div>

                        <div class="col-12">
                          <div class="row top5">

                            <div class="col-5">
                              <a class="btn btn_saiba_mais" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="Saiba Mais">
                                <i class="fas fa-plus-circle fa-lg right5" style="color:#178517;"></i>VER MAIS
                              </a>
                            </div>

                            <div class="col-7 text-right">
                              <a class="btn btn_verde_topo w-100" href="javascript:void(0);"  title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row1[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                <h6><span>  ORÇAMENTO  <i class="fas fa-shopping-cart fa-lg left5" style=""></i></span></h6>
                              </a>
                            </div>
                          </div>
                        </div>

                      </div>

                    </li>

                    <?php
                  }

                }
                ?>

              </ul>


            </div>

            <div class="custom-navigation">
              <a href="#" class="flex-prev"><i class="fas fa-chevron-circle-left fa-3x"></i></a>
              <!-- <div class="custom-controls-container"></div> -->
              <a href="#" class="flex-next"><i class="fas fa-chevron-circle-right fa-3x"></i></a>
            </div>



          </div>





        </div>


        <div class="row bottom50">
          <?php if (Url::getURL( 0 ) == "" or Url::getURL( 0 ) == "empresa" or Url::getURL( 0 ) == "servicos") :?>
            <div class="col-9 p-0 top20 text-center">
              <img  class="w-100" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_prod01.png" alt="">
            </div>

            <div class="col-3 text-right top5">
              <a class="btn btn-lg btn_cinza" href="<?php echo Util::caminho_projeto() ?>/produtos" role="button">VER TODAS</a>
            </div>
          <?php endif; ?>
        </div>
      </div>


    <?php endif; ?>
