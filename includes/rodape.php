
<div class="container-fluid rodape">
	<div class="row pb20">



		<div class="container">
			<div class="row top20">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-2 p-0">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<!-- MENU   ================================================================ -->
				<!-- ======================================================================= -->
				<div class="col-3">
					<ul class="nav justify-content-center">

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?> "
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVICOS
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contato"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO
							</a>
						</li>


						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" OR Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">EQUIPAMENTOS
							</a>
						</li>




						<li class="nav-item ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>


					</div>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->

					<div class="col-5 p-0">
						<h5><span>ATENDIMENTO</span></h5>

						<div class="row telefone_rodape top25">


							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->
							<div class="col-6 relativo">
								<i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
								<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?> <br>
								<span>LIGUE AGORA</span>
							</div>

							<?php if (!empty($config[telefone2])): ?>
								<div class="col-6 relativo">
									<i class="fab fa-whatsapp right10"></i>
									<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
									<span>WHATSAPP</span>

								</div>
							<?php endif;?>



							<?php if (!empty($config[telefone3])): ?>
								<div class="col-6">
									<i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
									<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
									<span>WHATSAPP</span>
								</div>
							<?php endif; ?>

							<?php if (!empty($config[telefone4])): ?>
								<div class="col-6">
									<i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
									<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
								</div>
							<?php endif; ?>
							<!-- ======================================================================= -->
							<!-- telefones  -->
							<!-- ======================================================================= -->

						</div>

						<?php if (!empty($config[endereco])): ?>
							<div class="col-12 top25 endereco_topo media p-0">
								<div class="media-left">
									<i class="fas fa-map-marker-alt right10"></i>
								</div>
								<div class="media-body">
									<p class="media-heading"><?php Util::imprime($config[endereco]); ?></p>
								</div>
							</div>
						<?php endif;?>

					</div>


					<div class="col-2 top30 text-center p-0 redes">
						<div class="">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>


						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->
						<div class="top20 text-center redes">
							<?php if ($config[google_plus] != "") { ?>
								<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
									<i class="fab fa-google-plus-g fa-2x"></i>
								</a>
							<?php } ?>

							<?php if ($config[facebook] != "") { ?>
								<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
									<i class="fab fa-facebook-square fa-2x"></i>
								</a>
							<?php } ?>

							<?php if ($config[instagram] != "") { ?>
								<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
									<i class="fab fa-instagram fa-2x"></i>
								</a>
							<?php } ?>
						</div>
						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->

					</div>


				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row rodape_verde ">
			<div class="col-12 text-center top10 bottom10">
				<h5 class="Open">© Copyright  BR TERRA LOCAÇÃO DE EQUIPAMENTOS</h5>
			</div>
		</div>
	</div>
