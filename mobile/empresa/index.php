<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12  localizacao-pagina text-center">
      <h4><?php Util::imprime($banner[legenda_1]); ?></h4>
      <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_home.png" alt="" height="2" width="127"></amp-img>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row">
    <div class="col-12 empresa_geral top10">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa",2) ?>
      <h6 ><?php Util::imprime($row[legenda]); ?></h6>
      <h5 ><?php Util::imprime($row[titulo]); ?></h5>
    </div>
    <div class="col-12 top15">
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>

  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn_verde"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      LIGUE AGORA
    </a>
  </div>


</div>


<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja_tambem.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
