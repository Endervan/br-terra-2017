<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>




  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
  .bg-interna{
    background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12  localizacao-pagina text-center">
      <h4><?php Util::imprime($banner[legenda_1]); ?></h4>
      <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_home.png" alt="" height="2" width="127"></amp-img>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->




  <div class="row">
    <div class="col-12 empresa_geral top10">
      <h6 class="mr-5">CONHEÇA MAIS</h6>
      <h5 ><?php Util::imprime($banner[legenda_2]); ?></h5>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->

  <div class="row">
    <?php
    $i=0;
    $result1 = $obj_site->select("tb_servicos");
    if (mysql_num_rows($result1) > 0) {

      while ($row1 = mysql_fetch_array($result1)) {

        ?>

        <div class="col-6 top20">
          <a   href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row1[imagem]); ?>"
            width="150"
            height="151"
            layout="responsive"
            alt="<?php echo Util::imprime($row[titulo]) ?>">
          </amp-img>
        </a>

        <div class="top5">
            <div class="desc_titulo_servicos"><h3 ><?php Util::imprime($row1[titulo]); ?></h3></div>
        </div>
        <div class="text-right top10">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row1[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_verde btn_servico col-8 col-offset-4">
            SAIBA MAIS
          </a>
        </div>


        </div>

        <?php
      }

    }
    ?>
  </div>
  <!-- ======================================================================= -->
  <!-- servicos gerais -->
  <!-- ======================================================================= -->

  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn_verde"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      LIGUE AGORA
    </a>
    </div>


  </div>



  </div>



  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja_tambem.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->




  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
