<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",8) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-11 ml-auto top200 titulo_geral bottom140">
        <h2><?php Util::imprime($banner[legenda_1]); ?></h2>
        <h4><?php Util::imprime($banner[legenda_2]); ?></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <div class="container-fluid fundo_contato">
    <div class="row">




      <div class="container top30">
        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="row ">


            <div class="col-4  titulo_equipamento">
              <div class="col-10 ml-auto" >
                <h6><span>TRABALHE CONOSCO</span></h6>
                <h3>PREENCHA SEUS DADOS</h3>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_contatos.jpg" alt="">


                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->

                <div class="ml-auto top20"> <button type="button" class="btn btn_verde_contato disabled">ATENDIMENTO <i class="fas fa-chevron-circle-down left10"></i> </button></div>


                <div class="row telefone_topo top25">
                  <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90"></i>
                  <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                </div>

                <?php if (!empty($config[telefone2])): ?>
                  <div class="row text-center telefone_topo top15">
                    <i class="fab fa-whatsapp right10 left15"></i>
                    <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                  </div>
                <?php endif;

                ?>



                <?php if (!empty($config[telefone3])): ?>
                  <div class="row text-center telefone_topo top15">
                    <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90" ></i>
                    <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
                  </div>
                <?php endif; ?>

                <?php if (!empty($config[telefone4])): ?>
                  <div class="row text-center telefone_topo top15">
                    <i class="fas fa-phone fa-fw right10 left15" data-fa-transform="rotate-90" ></i>
                    <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
                  </div>
                <?php endif; ?>
                <!-- ======================================================================= -->
                <!-- telefones  -->
                <!-- ======================================================================= -->




              </div>
            </div>



            <div class="col-8 fundo_formulario">

              <ul class="nav nav-pills nav-fill justify-content-center">
                <li class="nav-item mr-3">
                  <a class="nav-link " href="<?php echo Util::caminho_projeto() ?>/contato">FALE CONOSCO</a>
                </li>
                <li class="nav-item mr-3">
                  <a class="nav-link active" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php Util::imprime($config[src_place]); ?>" target="_blank">ONDE ESTAMOS</a>
                </li>

              </ul>


              <!--  ==============================================================  -->
              <!-- FORMULARIO CONTATOS-->
              <!--  ==============================================================  -->
              <div class=" col-12 p-5">
                <div class="form-row">
                  <div class="col">
                    <div class="form-group relativo">
                      <input type="text" name="assunto" class="form-control fundo-form" placeholder="ASSUNTO">
                      <span class="fas fa-user-circle form-control-feedback"></span>
                    </div>
                  </div>

                </div>

                <div class="form-row">
                  <div class="col">
                    <div class="form-group relativo">
                      <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME">
                      <span class="fas fa-user form-control-feedback"></span>
                    </div>
                  </div>

                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="email" class="form-control fundo-form" placeholder="E-MAIL">
                      <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="telefone" class="form-control fundo-form" placeholder="TELEFONE">
                      <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>
                    </div>
                  </div>

                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="escolaridade" class="form-control fundo-form" placeholder="ESCOLARIDADE">
                      <span class="fas fa-graduation-cap form-control-feedback"></span>
                    </div>
                  </div>
                </div>


                <div class="form-row">
                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="area" class="form-control fundo-form" placeholder="ÁREA">
                      <span class="fas fa-briefcase form-control-feedback"></span>
                    </div>
                  </div>

                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="cargo" class="form-control fundo-form" placeholder="CARGO">
                      <span class="fas fa-address-book form-control-feedback"></span>
                    </div>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="cidade" class="form-control fundo-form" placeholder="CIDADE">
                      <span class="fa fa-home form-control-feedback"></span>
                    </div>
                  </div>

                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="text" name="estado" class="form-control fundo-form" placeholder="ESTADO">
                      <span class="fa fa-globe form-control-feedback"></span>
                    </div>
                  </div>
                </div>


                <div class="form-row">

                  <div class="col">
                    <div class="form-group  relativo">
                      <input type="file" name="curriculo" class="form-control fundo-form" placeholder="CURRICULO">
                      <span class="fas fa-file form-control-feedback"></span>
                    </div>
                  </div>
                </div>


                <div class="form-row">
                  <div class="col">
                    <div class="form-group relativo">
                      <textarea name="mensagem" cols="15" rows="4" class="form-control fundo-form" placeholder="MENSAGEM"></textarea>
                      <span class="fas fa-pencil-alt form-control-feedback"></span>
                    </div>
                  </div>
                </div>




                <div class="col-12 mont text-right padding0 ">
                  <button type="submit" class="btn btn-lg btn_verde" name="btn_contato">
                    ENVIAR
                  </button>
                </div>

              </div>

              <div class="titulo_equipamento">
                <div class="col-7 top80 ml-auto" >
                  <h6><span>ONDE ESTAMOS</span></h6>
                  <h3>NOSSA LOCALIZAÇÃO</h3>
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo.png" alt="">
                </div>

              </div>


            </div>
          </form>
          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS-->
          <!--  ==============================================================  -->



        </div>
      </div>

    </div>
  </div>



  <div class="container-fluid mapa">
    <div class="row">

      <div class="container top30">
        <div class="row">


          <div class="col">
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="340" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- ======================================================================= -->
            <!-- mapa   -->
            <!-- ======================================================================= -->

          </div>

        </div>
      </div>

    </div>

  </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>






<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Email: ".$_POST[email]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo Pretedido: ".$_POST[cargo]." <br />
  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";


  if (Util::envia_email($config[email_trabalhe_conosco],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email])) {
    Util::envia_email($config[email_copia],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email]);
    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}



?>


<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fas fa-check',
      invalid: 'fas fa-times',
      validating: 'fas fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
