<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>)  center 56px no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-11 ml-auto top200 titulo_geral bottom140">
        <h2><?php Util::imprime($banner[legenda_1]); ?></h2>
        <h4><?php Util::imprime($banner[legenda_2]); ?></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container">
    <div class="row justify-content-md-center titulo_equipamento">
      <div class="col-8 top20">
        <h6><span>CONHEÇA MAIS </span></h6>
        <h3>NOSSOS <?php Util::imprime($banner[legenda_1]); ?></h3>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo.png" alt="">
      </div>
      <div class="col-2 p-0 top15">
        <a class="btn btn-default btn_cinza" href="<?php echo Util::caminho_projeto() ?>/orcamento" title="" >
          SOLICITE ORÇAMENTO
        </a>
      </div>
    </div>
  </div>



  <!-- SERVICOS -->
  <div class="container top50 bottom60">
    <div class="row">

      <?php
      $i=0;
      $result1 = $obj_site->select("tb_servicos");
      if (mysql_num_rows($result1) > 0) {
        while ($row1 = mysql_fetch_array($result1)) {
        ?>
          <div class="col-3 lista-servicos bottom15">
            <div class="card" style="width: 100%;">
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php echo Util::imprime($row1[url_amigavel]) ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 157, 129, array("class"=>"w-100 card-img-top", "alt"=>"$row1[titulo]")) ?>
              </a>
              <div class="card-body">
                <p class="card-text"><?php echo Util::imprime($row1[titulo]) ?></p>
              </div>
            </div>
            <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php echo Util::imprime($row1[url_amigavel]) ?>" class="btn top5 btn_verde float-right">SAIBA MAIS</a>
          </div>
        <?php
        }
      }
      ?>
    </div>
  </div>
  <!-- SERVICOS -->






  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/lista_produtos_categorias.php') ?>
  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">


$(window).load(function() {
  $('#slider_carousel_portifolio').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 240,
    itemMargin: 0,
  });
});


$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





$(window).load(function() {
  $('#slider1').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 190,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});



</script>
