<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-11 ml-auto top200 titulo_geral bottom140">
        <h2><?php Util::imprime($banner[legenda_1]); ?></h2>
        <h4><?php Util::imprime($banner[legenda_2]); ?></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row">


      <div class="col-5  ml-auto order-12 descricao_geral">
        <div >
          <h2><?php Util::imprime($dados_dentro[titulo]); ?></h2>
        </div>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_empresa.png" alt="">


        <div class="top30">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>

        <div class="top20">
          <a class="btn  btn-outline-success Open" title="SOLICITAR UM ORÇAMENTO"
          href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'" role="button">
          SOLICITE ORÇAMENTO  <i class="fas fa-shopping-cart fa-lg left5" style="position:absolute;right:0;top:0;background:#009700;color:#fff;padding:4px;width:37px;height:37px;"></i>
        </a>
      </div>



      <!-- TELEVENDAS -->
      <div class="top15">
        <div class="alert alert-success text-center" role="alert">
          <strong>TELEVENDAS</strong>
        </div>
        <div class="row">
          <div class="col-4 pg0 telefone_topo">
            <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
            <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
          </div>

          <?php if (!empty($config[telefone2])): ?>
            <div class="col-4 pg0 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
            </div>
          <?php endif; ?>

          <?php if (!empty($config[telefone3])): ?>
              <div class="col-4 pg0 text-center telefone_topo">
                  <i class="fab fa-whatsapp right10"></i>
                  <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
          <?php endif; ?>
        </div>
      </div>
    <!-- TELEVENDAS -->


    </div>


    <div class="col-6 padding0 order-1">


      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_produto_servico_dentro.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->


    </div>

  </div>
</div>





  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/lista_produtos_categorias.php') ?>
  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>



<script type="text/javascript">
$(window).load(function() {


  $('#carousel_item').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    itemWidth: 110,
    itemMargin: 0,
    asNavFor: '#slider_item'
  });

  $('#slider_item').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    sync: "#carousel_item"
  });


});



$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





$(window).load(function() {
  $('#slider1').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 190,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});



</script>
