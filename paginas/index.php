<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>





  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/lista_produtos_categorias.php') ?>
  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_servicos_empresa top30">
    <div class="row">

      <div class="container">
        <div class="row titulo_equipamento">


          <div class="col-12 text-center top20">
            <h6><span>CONFIRA NOSSA LINHA DE </span></h6>
            <h3>EQUIPAMENTOS</h3>
          </div>


          <!-- ======================================================================= -->
          <!-- NOSSOS SERVICOS  FLESLIDER+EFECT JULIA -TABEM TA QUEBRANDO LAYOUNT -VERIFICAR 1170   -->
          <!-- ======================================================================= -->
          <div class="col-11 ml-auto portfolio lista_produtos_home bottom20">
            <div id="slider_carousel_portifolio" class="flexslider w-75" >
              <ul class="slides">

                <?php
                $i=0;
                $result1 = $obj_site->select("tb_servicos","AND exibir_home = 'SIM'");
                if (mysql_num_rows($result1) > 0) {

                  while ($row1 = mysql_fetch_array($result1)) {

                    ?>

                    <li class="top20">

                      <div class="lista-produto">
                        <div class="grid">
                          <figure class="effect-julia">
                            <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 157, 129, array("class"=>"w-100", "alt"=>"$row1[titulo]")) ?>
                            <figcaption>
                              <div class="hover">
                                <p class="col-12 text-center">
                                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/servico/<?php echo Util::imprime($row1[url_amigavel]) ?>">
                                    <i class="fas fa-plus-circle fa-2x"></i>
                                  </a>
                                </p>
                              </div>
                            </figcaption>
                          </figure>

                        </div>
                        <div class="desc_servicos">
                          <h5 class="text-left"><span><?php echo Util::imprime($row1[titulo],41) ?></span></h5>
                        </div>
                      </div>
                    </li>

                    <?php
                  }

                }
                ?>

              </ul>


            </div>




          </div>

          <div class="col-12 text-center top30">
            <a class="btn btn_verde" href="<?php echo Util::caminho_projeto() ?>/servicos/">
              SAIBA MAIS
            </a>

          </div>

          <!-- ======================================================================= -->
          <!-- NOSSOS SERVICOS  FLESLIDER+EFECT JULIA -TABEM TA QUEBRANDO LAYOUNT -VERIFICAR 1170   -->
          <!-- ======================================================================= -->



          <div class="col-6">
            <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
              <?php if (!empty($row1[legenda])): ?>
                <h6 ><span><?php Util::imprime($row1[legenda]); ?></span></h6>
              <?php endif; ?>
              <h3 >NOSSA <?php Util::imprime($row1[titulo]); ?></h3>
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_titulo_empresa.png" alt="">

              <div class="top20">
                  <p ><?php Util::imprime($row1[descricao],608); ?></p>
              </div>

              <div class="text-right top50">
                <a class="btn btn_verde" href="<?php echo Util::caminho_projeto() ?>/empresa">
                  CONHEÇA MAIS
                </a>
              </div>


          </div>

        </div>
      </div>


    </div>
  </div>







  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
      delay:9000
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">


$(window).load(function() {
  $('#slider_carousel_portifolio').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 200,
    itemMargin: 0,
  });
});


$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 352,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





$(window).load(function() {
  $('#slider1').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 190,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});



</script>
